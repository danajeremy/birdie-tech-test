"use strict";

let express = require('express');
let bodyParser = require('body-parser');
let app = express();
let http = require('http');
let server = http.Server(app);
let mysql = require('mysql');

let connection = mysql.createConnection({
    host     : 'birdie-test.cyosireearno.eu-west-2.rds.amazonaws.com',
    user     : 'xxxx',
    password : 'xxxx',
    database : 'birdietest'
});

let port = process.env.PORT || 8282;

// Start the Server
server.listen(port, function () {
    console.log('Server Started. Listening on *:' + port);
    connection.connect();
});

// Express Middleware
app.use(express.static('public'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({
  limit: '50mb',
  extended: true
}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Content-Type", "application/json");
    next();
});

/* *************** API CALL *************** */

app.get('/get-columns', function(req, res) {
    let columnsArray = new Array();
    connection.query('SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "census_learn_sql" AND TABLE_SCHEMA = "birdietest"', function (error, results, fields) {
        if (error) throw error;
        results.forEach(element => columnsArray.push(element.COLUMN_NAME));
        //console.log(columnsArray);
        res.send({
			"columns": columnsArray
	    });
    });
});

app.post('/get-data', function(req, res) {
	//Getting the param from the request
    let columnName = req.body.columnName;
    console.log("columnName", columnName);

    let data = Array();
    connection.query('SELECT `' + columnName + '` AS col, COUNT(`' + columnName + '`) AS count, AVG(age) AS avg_age FROM census_learn_sql GROUP BY col ORDER BY count DESC', function (error, results, fields) {
        if (error) throw error;
        //console.log(results);
        results.forEach(element => data.push({col: element.col, count: element.count.toString(), avg: element.avg_age}));
        //console.log("array", data);
        res.send({
            "data": data
        });
    });
});
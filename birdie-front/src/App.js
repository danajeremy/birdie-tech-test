import React, { Component } from 'react';
import Select from './Select';
import Table from './Table';
import './App.css';
import axios from 'axios';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      categoriesValues : [],
      selectedValue: '',
      dataForACategory: [],
      loadingState: 'No category selected yet',
      nonDisplayedRowsCounter: 0
    };
  }

  componentDidMount(){
    let that = this;
    axios.get('http://localhost:8282/get-columns')
    .then(function (response) {
      that.setState({categoriesValues: response.data.columns})
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  selectedData = (dataFromChild) => {
    console.log("dataFromChild", dataFromChild);

    this.setState({
      selectedValue: dataFromChild,
      loadingState: "Fetching data...",
      dataForACategory: [],
      nonDisplayedRowsCounter: 0
    })

    let that = this;

    axios.post('http://localhost:8282/get-data', {
      columnName: dataFromChild
    })
    .then(function (response) {
      console.log("response", response);

      //Getting only the first 100 rows
      let data = response.data.data.slice(0, 100);

      that.setState({
        loadingState: '',
        dataForACategory: data,
        nonDisplayedRowsCounter: (response.data.data.length > 100) ? response.data.data.length - 100 : 0
      })
    })
    .catch(function (error) {
      console.log(error);
    });

  };

  render() {
    return (
      <div>
        <Select data={this.state.categoriesValues} callbackFromParent={this.selectedData}/>
        <p>{this.state.loadingState}</p>
        <Table category={this.state.selectedValue} data={this.state.dataForACategory}/>
        <p>Non displayed rows: {this.state.nonDisplayedRowsCounter}</p>
      </div>
    );
  }
}

export default App;

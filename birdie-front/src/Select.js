import React, { Component } from 'react';
import './App.css';

class Select extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };

    this.handleChange = this.handleChange.bind(this);
  }

  upperWord(string){
    return string.toLowerCase().replace(/\b[a-z]/g, function(letter) {
      return letter.toUpperCase();
    });
  }

  handleChange(e){
    this.props.callbackFromParent(e.target.value);
  }

  render() {
    let catList = this.props.data.map((item) => {
      return(
        <option key={item} value={item}>{this.upperWord(item)}</option>
      );
    });

    if(catList.length === 0){
      return (
        <div>
          <p>Loading categories...</p>
        </div>
      )
    }else{
      return (
        <div style={{width: 300}}>
            <div class="form-group">
              <label>Category</label>
              <select class="form-control" onChange={this.handleChange}>
                <option selected disabled>Select a category</option>
                {catList}
              </select>
            </div>
        </div>
      );
    }
  }
}

export default Select;

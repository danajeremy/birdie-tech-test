import React, { Component } from 'react';
import './App.css';

class Table extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  upperWord(string){
    return string.toLowerCase().replace(/\b[a-z]/g, function(letter) {
      return letter.toUpperCase();
    });
  }

  render() {
    let rows = this.props.data.map((item, index) => {
      return(
        <tr key={item.col}>
          <td>{index + 1}</td>
          <td>{item.col}</td>
          <td>{item.count}</td>
          <td>{item.avg}</td>
        </tr>
      );
    });

    if(rows.length !== 0){
      return (
        <div>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>{this.upperWord(this.props.category)}</th>
                <th>Count</th> 
                <th>Average Age</th>
              </tr>
            </thead>
            <tbody>
              {rows}
            </tbody>
          </table>
        </div>
      )
    }else{
      return (
        <div></div>
      )
    }
  }
}

export default Table;
